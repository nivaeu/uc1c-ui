# Introduction

This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for further information.
A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## NIVA UC1C User Interface

A graphical user interface for farmer performance.

## Development

### Requirements

- Node v14.6.1+
- Npm

### Google Maps Api

A one part of the application uses Google Maps Api to display agricultural parcels on a map. The Google Maps API is free with a limited usage. The Maps JavaScript API key can be generated in the [Google Developers Console](https://console.developers.google.com). The Maps JavaScript API key should be added to .env file located in the root directory (as the value of REACT_APP_GOOGLE_MAPS_API).

Example (not a working API key):

```sh
REACT_APP_GOOGLE_MAPS_API=SIzaSiBSendkG0Ask2qLAib8rpnn39TIPoeU4P5
```

To generate the api key in Google Developers Console:
1. Open https://console.developers.google.com
2. Add a new project
3. From + ENABLE APIS AND SERVICES and choose Maps JavaScript API and click Enable
4. Google will ask to set up a billing account, it must be completed to get access to the Javascript API, card won't be actually charged, so google claims.
5. Go to the maps api credientials page https://console.cloud.google.com/google/maps-apis/credentials and clip Maps API Key to get the API key

After testing it would be smart to disable billing and API's, just in case, https://console.cloud.google.com/billing

### Install dependencies

`npm install`

### Start the application

build:
`npm run build`

run:
`npm install serve -g && serve -s build`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

There is initially one farmer in the example data, with the name **KLIENT ID538999** and persional code **39107184489**

### Tests

`npm test`
