/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

export const handleChartsPairOneMinMaxMize = ({
  index,
  minimized,
}: {
  index: number;
  minimized: boolean;
}) => ({
  type: 'HANDLE_PAIR_ONE',
  index,
  minimized,
});

export const handleChartsPairTwoMinMaxMize = ({
  index,
  minimized,
}: {
  index: number;
  minimized: boolean;
}) => ({
  type: 'HANDLE_PAIR_TWO',
  index,
  minimized,
});

export const handleResetChartsToDefaultState = () => ({
  type: 'RESET_CHARTS_TO_DEFAULT_STATE',
});
