/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

export const setClient = ({
  name,
  id,
  animals,
  subsidies,
  parcels,
  plantChemicals,
  plantFertilizers,
  economicInfo,
  livestockLoadData,
  agrotechnics,
}: {
  name: string;
  id: string;
  animals: any;
  subsidies: any;
  parcels: any;
  plantChemicals: any;
  plantFertilizers: any;
  economicInfo: any;
  livestockLoadData: any;
  agrotechnics: any;
}) => ({
  type: 'SET_CLIENT',
  name,
  id,
  animals,
  subsidies,
  parcels,
  plantChemicals,
  plantFertilizers,
  economicInfo,
  livestockLoadData,
  agrotechnics,
});

export const setParcels = ({ parcels }: { parcels: any }) => ({
  type: 'SET_PARCELS',
  parcels,
});
