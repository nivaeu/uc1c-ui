/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

export function countCharacters(character: string, string: string) {
  return string
    .split('')
    .reduce((acc, ch) => (ch === character ? acc + 1 : acc), 0);
}

export function calculatePatternIndex(
  string: string,
  patternToLookFor: string,
  occurenceNumber: number,
) {
  var l = string.length,
    i = -1;
  while (occurenceNumber-- && i++ < l) {
    i = string.indexOf(patternToLookFor, i);
    if (i < 0) break;
  }
  return i;
}

interface Units {
  period: string;
}

export function getPossiblePeriods(units: Units[]) {
  let periods: string[] = [];

  units.forEach((unit: any) => {
    if (!periods.includes(unit.period)) {
      periods = [...periods, unit.period];
    }
  });

  return periods.sort((a, b) => {
    const periodA = parseInt(a);
    const periodB = parseInt(b);
    if (periodA < periodB) {
      return 1;
    } else if (periodA > periodB) {
      return -1;
    } else {
      return 0;
    }
  });
}

export function getCurrentPeriod(units: Units[]) {
  let periods: string[] = [];

  units.forEach((unit: any) => {
    if (!periods.includes(unit.period)) {
      periods = [...periods, unit.period];
    }
  });

  const possiblePeriods = periods.sort((a, b) => {
    const periodA = parseInt(a);
    const periodB = parseInt(b);
    if (periodA < periodB) {
      return 1;
    } else if (periodA > periodB) {
      return -1;
    } else {
      return 0;
    }
  });

  return possiblePeriods[0];
}

export function calculatePercentageOfNumber(
  partialValue: string,
  totalValue: string,
) {
  return ((100 * parseFloat(partialValue)) / parseFloat(totalValue)).toFixed(2);
}
