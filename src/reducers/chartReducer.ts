/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { AnyAction } from 'redux';

export const chartReducerDefaultState = {
  chartPairOneMinimized: true,
  maxiMizedChartsForPairOne: [],
  chartPairTwoMinimized: true,
  maxiMizedChartsForPairTwo: [],
};

export const chartReducer = (
  state = chartReducerDefaultState,
  action: AnyAction,
) => {
  switch (action.type) {
    case 'HANDLE_PAIR_ONE': {
      const chartIndex = state.maxiMizedChartsForPairOne.findIndex(
        (index: number) => index === action.index,
      );

      if (!action.minimized && state.chartPairOneMinimized) {
        return {
          ...state,
          chartPairOneMinimized: false,
          maxiMizedChartsForPairOne: state.maxiMizedChartsForPairOne.concat(
            action.index,
          ),
        };
      }
      if (
        !action.minimized &&
        !state.chartPairOneMinimized &&
        chartIndex === -1
      ) {
        return {
          ...state,
          maxiMizedChartsForPairOne: state.maxiMizedChartsForPairOne.concat(
            action.index,
          ),
        };
      } else if (action.minimized && !state.chartPairOneMinimized) {
        if (chartIndex !== 1 && state.maxiMizedChartsForPairOne.length === 1) {
          return {
            ...state,
            chartPairOneMinimized: true,
            maxiMizedChartsForPairOne: [],
          };
        } else if (state.maxiMizedChartsForPairOne.length === 2) {
          return {
            ...state,
            maxiMizedChartsForPairOne: state.maxiMizedChartsForPairOne.filter(
              (index) => index !== action.index,
            ),
          };
        } else {
          return state;
        }
      } else {
        return state;
      }
    }
    case 'HANDLE_PAIR_TWO': {
      const chartIndex = state.maxiMizedChartsForPairTwo.findIndex(
        (index: number) => index === action.index,
      );

      if (!action.minimized && state.chartPairTwoMinimized) {
        return {
          ...state,
          chartPairTwoMinimized: false,
          maxiMizedChartsForPairTwo: state.maxiMizedChartsForPairTwo.concat(
            action.index,
          ),
        };
      }
      if (
        !action.minimized &&
        !state.chartPairTwoMinimized &&
        chartIndex === -1
      ) {
        return {
          ...state,
          maxiMizedChartsForPairTwo: state.maxiMizedChartsForPairTwo.concat(
            action.index,
          ),
        };
      } else if (action.minimized && !state.chartPairTwoMinimized) {
        if (chartIndex !== 1 && state.maxiMizedChartsForPairTwo.length === 1) {
          return {
            ...state,
            chartPairTwoMinimized: true,
            maxiMizedChartsForPairTwo: [],
          };
        } else if (state.maxiMizedChartsForPairTwo.length === 2) {
          return {
            ...state,
            maxiMizedChartsForPairTwo: state.maxiMizedChartsForPairTwo.filter(
              (index) => index !== action.index,
            ),
          };
        } else {
          return state;
        }
      } else {
        return state;
      }
    }
    case 'RESET_CHARTS_TO_DEFAULT_STATE': {
      return {
        ...chartReducerDefaultState,
      };
    }
    default:
      return state;
  }
};
