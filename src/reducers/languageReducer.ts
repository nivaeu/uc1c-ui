/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { AnyAction } from 'redux';

export const defaultLanguage = localStorage.getItem('i18nextLng') || 'en';

export const languageReducerDefaultState = {
  language: defaultLanguage,
};

export const languageReducer = (
  state = languageReducerDefaultState,
  action: AnyAction,
) => {
  switch (action.type) {
    case 'SET_LANG': {
      return {
        ...state,
        language: action.language,
      };
    }
    default:
      return state;
  }
};
