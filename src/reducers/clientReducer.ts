/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { AnyAction } from 'redux';

interface ClientReducerDefaultState {
  name: string;
  id: string;
  parcels: any;
  animals: any;
  subsidies: any;
  plantChemicals: any;
  plantFertilizers: any;
  economicInfo: any;
  livestockLoadData: any;
  agrotechnics: any;
}

export const clientReducerDefaultState: ClientReducerDefaultState = {
  name: '',
  id: '',
  parcels: {},
  animals: {},
  subsidies: {},
  plantChemicals: {},
  plantFertilizers: {},
  economicInfo: {},
  livestockLoadData: {},
  agrotechnics: {},
};

export const clientReducer = (
  state = clientReducerDefaultState,
  action: AnyAction,
) => {
  switch (action.type) {
    case 'SET_CLIENT': {
      return {
        ...state,
        name: action.name,
        id: action.id,
        parcels: action.parcels,
        animals: action.animals,
        subsidies: action.subsidies,
        plantChemicals: action.plantChemicals,
        plantFertilizers: action.plantFertilizers,
        economicInfo: action.economicInfo,
        livestockLoadData: action.livestockLoadData,
        agrotechnics: action.agrotechnics,
      };
    }
    case 'SET_PARCELS': {
      return {
        ...state,
        parcels: action.parcels,
      };
    }
    default:
      return state;
  }
};
