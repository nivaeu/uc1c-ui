/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { languageReducer } from '../../reducers/languageReducer';

it('Should set languageReducer to default state', () => {
  const state = languageReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual({ language: 'en' });
});

it('Should handle SET_LANG correctly', () => {
  const originalState = {
    language: 'en',
  };
  const language = 'et';
  const state = languageReducer(originalState, {
    type: 'SET_LANG',
    language,
  });
  expect(state).toEqual({ language });
});
