/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { clientReducer } from '../../reducers/clientReducer';

it('Should set clientReducer to default state', () => {
  const state = clientReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual({
    name: '',
    id: '',
    parcels: {},
    animals: {},
    subsidies: {},
    economicInfo: {},
    plantFertilizers: {},
    plantChemicals: {},
    livestockLoadData: {},
    agrotechnics: {},
  });
});

it('Should handle SET_CLIENT correctly', () => {
  const originalState = clientReducer(undefined, { type: '@@INIT' });
  const name = 'Test Client';
  const id = '123';

  const state = clientReducer(originalState, {
    type: 'SET_CLIENT',
    name,
    id,
  });

  expect(state).toEqual({
    name,
    id,
  });
});
