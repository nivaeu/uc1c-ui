/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { setLanguage } from '../../actions/languageActions';

it('Should setup setLanguage action object', () => {
  const action = setLanguage({ language: 'et' });
  expect(action).toEqual({ type: 'SET_LANG', language: 'et' });
});
