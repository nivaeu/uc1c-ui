/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { withTranslation, WithTranslation } from 'react-i18next';

type LivestockProps = WithTranslation;

const Livestock = (props: LivestockProps) => {
  const { t } = props;

  return (
    <div className="row livestock-container">
      <h1 className="livestock-container__heading">{t('livestock.title')}</h1>

      <p className="livestock-container__paragraph">
        {t('livestock.paragraphOne')}
      </p>

      <p className="livestock-container__paragraph">
        {t('livestockTable.paragraphOne')}
      </p>

      <table className="table">
        <tbody>
          <tr className="table__row table__row--green">
            <th className="table__header-cell">
              {t('livestockTable.headerOne')}
            </th>
            <th className="table__header-cell">
              {t('livestockTable.headerTwo')}
            </th>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellOneDataOne')}
            </td>
            <td className="table__data-cell">1.00</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellTwoDataOne')}
            </td>
            <td className="table__data-cell">0.60</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellThreeDataOne')}
            </td>
            <td className="table__data-cell">0.14</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellFourDataOne')}
            </td>
            <td className="table__data-cell">0.11</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellFiveDataOne')}
            </td>
            <td className="table__data-cell">0.49</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellSixDataOne')}
            </td>
            <td className="table__data-cell">0.34</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellSevenDataOne')}
            </td>
            <td className="table__data-cell">0.03</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellEightDataOne')}
            </td>
            <td className="table__data-cell">0.006</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellNineDataOne')}
            </td>
            <td className="table__data-cell">0.20</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellTenDataOne')}
            </td>
            <td className="table__data-cell">0.07</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellElevenDataOne')}
            </td>
            <td className="table__data-cell">0.51</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellTwelveDataOne')}
            </td>
            <td className="table__data-cell">0.38</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellThirteenDataOne')}
            </td>
            <td className="table__data-cell">0.06</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellFourteenDataOne')}
            </td>
            <td className="table__data-cell">0.21</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellFifteenDataOne')}
            </td>
            <td className="table__data-cell">0.21</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellSixteenDataOne')}
            </td>
            <td className="table__data-cell">0.37</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellSeventeenDataOne')}
            </td>
            <td className="table__data-cell">0.03</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('livestockTable.dataCellEighteenDataOne')}
            </td>
            <td className="table__data-cell">0.03</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default withTranslation()(Livestock);
