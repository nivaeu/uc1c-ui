/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { withTranslation, WithTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { RootState } from '../App';
import { languageReducerDefaultState } from '../reducers/languageReducer';

type AboutProps = {
  languageState: typeof languageReducerDefaultState;
} & WithTranslation;

const About = (props: AboutProps) => {
  const { t, languageState } = props;

  return (
    <div className="row about-container">
      <div className="about-container-logo-box">
        <img
          className="about-container-logo-box__image"
          src={process.env.PUBLIC_URL + '/assets/images/oats.png'}
          alt="niva"
        />
      </div>

      <h1 className="about-container__heading">{t('about.title')}</h1>

      {languageState.language === 'en' ? (
        <div>
          <p className="about-container__paragraph">
            {t('about.paragraphOne')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphTwo')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphThree')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphFour')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphFourLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphFourLinkText')}
            </a>
            .
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphFive')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphSix')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphSixLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphSixLinkText')}
            </a>
            .
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphSeven')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphEight')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphNine')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphNineLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphNineLinkText')}
            </a>
            .
          </p>

          <p className="about-container__paragraph about-container__paragraph--large-margin">
            {t('about.paragraphTen')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphTenLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphTenLinkText')}
            </a>{' '}
            {t('about.paragraphTenSecondPart')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphEleven')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphElevenLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphElevenLinkText')}
            </a>
            .
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphTwelve')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphThirteen')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphThirteenLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphThirteenLinkText')}
            </a>
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphFourteen')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphFourteenLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphFourteenLinkText')}
            </a>
          </p>
        </div>
      ) : (
        <div>
          <p className="about-container__paragraph">
            {t('about.paragraphOne')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphTwo')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphThree')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphThreeLinKText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphThreeLinKText')}
            </a>{' '}
            {t('about.paragraphThreeSecondPart')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphThreeSecondPartLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphThreeSecondPartLinkText')}
            </a>
            .
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphFour')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphFive')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphSix')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphSeven')}
          </p>

          <p className="about-container__paragraph about-container__paragraph--large-margin">
            {t('about.paragraphEight')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphEightLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphEightLinkText')}
            </a>{' '}
            {t('about.paragraphEightSecondPart')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphNine')} (
            <a
              className="about-container__link"
              href={t('about.paragraphNineLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphNineLinkText')}
            </a>
            ).
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphTen')}
          </p>

          <p className="about-container__paragraph">
            {t('about.paragraphEleven')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphElevenLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphElevenLinkText')}
            </a>
          </p>
          <p className="about-container__paragraph">
            {t('about.paragraphTwelve')}{' '}
            <a
              className="about-container__link"
              href={t('about.paragraphTwelveLinkText')}
              target="_blank"
              rel="noreferrer"
            >
              {t('about.paragraphTwelveLinkText')}
            </a>
          </p>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  languageState: state.languageReducer,
});

export default connect(mapStateToProps)(withTranslation()(About));
