/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { useState } from 'react';
import { withTranslation, WithTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useScrollPosition } from '@n8tb1t/use-scroll-position';

type HeaderProps = WithTranslation;

const Header = (props: HeaderProps) => {
  const { t } = props;
  const [hideHeaderLogo, setHideHeaderLogo] = useState(false);

  useScrollPosition(({ currPos }) => {
    if (currPos.y <= -150) {
      setHideHeaderLogo(true);
    } else {
      setHideHeaderLogo(false);
    }
  });

  return (
    <header className="header-container">
      <div className="logo-box">
        <img
          className="logo-box__image"
          src={process.env.PUBLIC_URL + '/assets/images/niva-logo.png'}
          alt="niva"
        />
      </div>

      <nav
        className={
          hideHeaderLogo
            ? 'navigation-box navigation-box--sticky'
            : 'navigation-box'
        }
      >
        {hideHeaderLogo && (
          <div className="logo-box-white">
            <img
              className="logo-box-white__image"
              src={
                process.env.PUBLIC_URL + '/assets/images/niva-logo-white.png'
              }
              alt="niva"
            />
          </div>
        )}

        <ul className="navigation-list">
          <li className="navigation-list__item">
            <Link className="navigation-list__link" to="/">
              {t('header.home')}
            </Link>
          </li>
          <li className="navigation-list__item">
            <Link
              className="navigation-list__link"
              to={`/${t('navigation.about')}`}
            >
              {t('header.about')}
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default withTranslation()(Header);
