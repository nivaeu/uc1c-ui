/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { withTranslation, WithTranslation } from 'react-i18next';
import { RouteComponentProps } from 'react-router-dom';
import Button from '../common/buttons/Button';

type NotFoundProps = {
  history: RouteComponentProps['history'];
} & WithTranslation;

const NotFound = (props: NotFoundProps) => {
  const { t } = props;

  const handleGoBackToPreviousPage = () => {
    props.history.goBack();
  };

  return (
    <div className="row not-found-container">
      <h1 className="not-found-container__heading">{t('notFound.title')}</h1>
      <p className="not-found-container__paragraph">
        {t('notFound.paragraph')}.
      </p>

      <div className="not-found-container__button-container">
        <Button onClick={handleGoBackToPreviousPage} variant="contained">
          {t('notFound.buttonText')}
        </Button>
      </div>
    </div>
  );
};

export default withTranslation()(NotFound);
