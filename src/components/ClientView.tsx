/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { withTranslation, WithTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { RootState, AppDispatch } from '../App';
import { clientReducerDefaultState } from '../reducers/clientReducer';
import { calculatePatternIndex } from '../utils/appUtils';
import { setClient } from '../actions/clientActions';
import { handleResetChartsToDefaultState } from '../actions/chartActions';
import Button from '../common/buttons/Button';
import Chart from './Chart';
import { ClickAwayListener } from '@material-ui/core';
import MaterialTooltip from '../common/tooltips/Tooltip';
import { chartReducerDefaultState } from '../reducers/chartReducer';
import { languageReducerDefaultState } from '../reducers/languageReducer';
import { getCurrentPeriod } from '../utils/appUtils';

type ClientViewProps = RouteComponentProps &
  WithTranslation & {
    dispatch: AppDispatch;
    clientState: typeof clientReducerDefaultState;
    chartState: typeof chartReducerDefaultState;
    languageState: typeof languageReducerDefaultState;
  };

type EconomicInfo = {
  economicClass: string;
  productionType: any;
};

type ClientViewState = {
  clientNotFoundError: string;
  classInfoToolTipOpen: boolean;
  economicInfo: EconomicInfo;
};

class ClientView extends React.Component<ClientViewProps, ClientViewState> {
  state = {
    clientNotFoundError: '',
    classInfoToolTipOpen: false,
    economicInfo: {
      economicClass: '',
      productionType: {
        productionTypeEn: '',
        productionTypeEt: '',
      },
    },
  };

  fetchClientDataIfNeeded = async () => {
    if (!this.props.clientState.name) {
      const pathname = this.props.location.pathname;
      const secondSlashIndex = calculatePatternIndex(pathname, '/', 2);
      const clientId = pathname.substring(secondSlashIndex + 1);
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/farmer?farmerId=${clientId}&limit=1`,
      );

      if (response.status === 200) {
        const responseJSON = await response.json();
        this.props.dispatch(handleResetChartsToDefaultState());
        this.props.dispatch(
          setClient({
            ...responseJSON,
          }),
        );
      } else {
        this.setState({
          clientNotFoundError: 'clientView.clientNotFoundError',
        });
      }
    }
  };

  convertFirstLettersToUpperCase = (name: string) => {
    return name
      .toLowerCase()
      .split(' ')
      .map((word) => {
        if (word === 'oü' || word === 'OÜ') {
          return word.toUpperCase();
        } else {
          return word.charAt(0).toUpperCase() + word.slice(1);
        }
      })
      .join(' ');
  };

  handleClientChange = () => {
    this.props.history.push('/');
  };

  handleTooltipForClassInfo = () => {
    this.setState((prevState) => ({
      classInfoToolTipOpen: !prevState.classInfoToolTipOpen,
    }));
  };

  handleTooltipCloseForClassInfo = () => {
    this.setState(() => ({ classInfoToolTipOpen: false }));
  };

  handleSetEconomicInfo = () => {
    if (!this.state.clientNotFoundError) {
      const economicInfo = this.props.clientState.economicInfo
        .economicInfoByYear;
      const currentPeriod = getCurrentPeriod(economicInfo);

      const currentPeriodEconomicInfo = economicInfo
        .filter((economicClass: any) => {
          return economicClass.period === currentPeriod;
        })
        .pop();

      this.setState(() => ({
        economicInfo: {
          economicClass: currentPeriodEconomicInfo.economicClass,
          productionType: {
            productionTypeEn: currentPeriodEconomicInfo.productionTypeEn,
            productionTypeEt: currentPeriodEconomicInfo.productionTypeEt,
          },
        },
      }));
    }
  };

  componentDidMount = async () => {
    await this.fetchClientDataIfNeeded();
    this.handleSetEconomicInfo();
  };

  render() {
    const { t, clientState } = this.props;
    const chartPairOneMinimized = this.props.chartState.chartPairOneMinimized;
    const chartPairTwoMinimized = this.props.chartState.chartPairTwoMinimized;

    const groupSize = 2;
    const charts = chartItems
      .map((item, index) => {
        return (
          <Chart
            title={t(item.title)}
            colors={item.colors}
            chartIndex={index}
            key={index}
          />
        );
      })
      .reduce((acc: any, curr, index) => {
        index % groupSize === 0 && acc.push([]);
        acc[acc.length - 1].push(curr);
        return acc;
      }, [])
      .map((content: any, index: number) => {
        return (
          <div
            className={
              (index === 0 && !chartPairOneMinimized) ||
              (index === 1 && !chartPairTwoMinimized)
                ? 'charts-pair--maximixed'
                : 'charts-pair'
            }
            key={index}
          >
            {content}
          </div>
        );
      });

    return (
      <div className="row client-view-container">
        {this.state.clientNotFoundError ? (
          <p className="client-view-container__not-found-error">
            {t(`${this.state.clientNotFoundError}`)}
          </p>
        ) : (
          <div className="client-view-header-box">
            <div className="client-view-name-class-container">
              <span className="client-view-name-class-container__text">
                {this.convertFirstLettersToUpperCase(clientState.name)}
              </span>

              <span className="client-view-name-class-container__text client-view-name-class-container__text--group">
                {`${t('economicInfo.productionType')} ${
                  this.props.languageState.language === 'en'
                    ? this.state.economicInfo.productionType.productionTypeEn
                    : this.state.economicInfo.productionType.productionTypeEt
                }`}
              </span>

              <div className="class-container">
                <span className="class-container__text client-view-name-class-container__text client-view-name-class-container__text--group">
                  {`${t('economicInfo.economicClass')} ${
                    this.state.economicInfo.economicClass
                  }`}
                </span>
                <ClickAwayListener
                  onClickAway={this.handleTooltipCloseForClassInfo}
                >
                  <div>
                    <MaterialTooltip
                      title={
                        <div>
                          {t('classes.tooltipText')}{' '}
                          {
                            <Link
                              className="tooltip-link"
                              to={`${t('paths.classes')}`}
                              target="_blank"
                            >
                              {t('classes.tooltipLinkText')}.
                            </Link>
                          }
                        </div>
                      }
                      handleToolTip={this.handleTooltipForClassInfo}
                      open={this.state.classInfoToolTipOpen}
                      ariaLabel="class info"
                    />
                  </div>
                </ClickAwayListener>
              </div>
            </div>

            <div className="client-view-header-box__button-container">
              <Button onClick={this.handleClientChange} variant="contained">
                {t('clientView.changeClientButton')}
              </Button>
            </div>
          </div>
        )}

        {!t(`${this.state.clientNotFoundError}`) && (
          <div className="charts-container">{charts}</div>
        )}
      </div>
    );
  }
}

const colorsOne = [
  '#07a84c',
  '#1a7240',
  '#32CD32',
  '#90EE90',
  '#008000',
  '#adff2f',
];
const colorsTwo = [
  '#FF8C00',
  '#008000',
  '#8B0000',
  '#07a84c',
  '#0077b6',
  '#03045e',
];
const colorsThree = [
  '#00b4d8',
  '#0077b6',
  '#03045e',
  '#80daeb',
  '#4166f5',
  '#000039',
];
const colorsFour = [
  '#800000',
  '#FF8C00',
  '#FFD700',
  '#B8860B',
  '#FF4500',
  '#8b4513',
];

const chartItems = [
  {
    title: 'charts.parcelsTitle',
    colors: colorsOne,
  },
  {
    title: 'charts.animalsTitle',
    colors: colorsTwo,
  },
  {
    title: 'charts.subsidiesTitle',
    colors: colorsThree,
  },
  {
    title: 'charts.farmerDashboardTitle',
    colors: colorsFour,
  },
];

const mapStateToProps = (state: RootState) => ({
  clientState: state.clientReducer,
  chartState: state.chartReducer,
  languageState: state.languageReducer,
});

export default connect(mapStateToProps)(withTranslation()(ClientView));
