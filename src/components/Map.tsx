/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import React from 'react';
import { GoogleMap, Polygon, useJsApiLoader } from '@react-google-maps/api';

const googleMapsContainerStyles = {
  width: '100%',
  height: '45rem',
};

export const googleMapsDefaultPosition = {
  lng: 24.737578222170168,
  lat: 59.04104264086484,
};

interface MapProps {
  multiParcelCoordinates?: any[];
  singleParcelCoordinates?: Array<{
    lng: number;
    lat: number;
  }>;
}

const Map = (props: MapProps) => {
  const { multiParcelCoordinates, singleParcelCoordinates } = props;
  const googleMapsApiKey =
    (process.env.REACT_APP_GOOGLE_MAPS_API &&
      process.env.REACT_APP_GOOGLE_MAPS_API) ||
    '';

  const { isLoaded, loadError } = useJsApiLoader({
    googleMapsApiKey,
    preventGoogleFontsLoading: true,
  });

  return (
    <div className="map-container">
      {isLoaded && (
        <GoogleMap
          mapContainerStyle={googleMapsContainerStyles}
          center={
            multiParcelCoordinates && multiParcelCoordinates.length !== 0
              ? multiParcelCoordinates[0][0]
              : singleParcelCoordinates && singleParcelCoordinates.length !== 0
              ? singleParcelCoordinates[0]
              : googleMapsDefaultPosition
          }
          zoom={multiParcelCoordinates ? 12 : 14}
        >
          {multiParcelCoordinates &&
            multiParcelCoordinates.length !== 0 &&
            multiParcelCoordinates.map((coordinate: any, index: number) => {
              return <Polygon paths={coordinate} key={index}></Polygon>;
            })}

          {singleParcelCoordinates && singleParcelCoordinates.length !== 0 && (
            <Polygon paths={singleParcelCoordinates}></Polygon>
          )}
        </GoogleMap>
      )}

      {loadError && <p className="map-container__error">Failed to load map.</p>}
    </div>
  );
};

export default React.memo(Map);
