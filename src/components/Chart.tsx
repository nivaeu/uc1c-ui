/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import React from 'react';
import {
  PieChart,
  Pie,
  Cell,
  ResponsiveContainer,
  BarChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Bar,
  LineChart,
  Line,
  Text,
} from 'recharts';
import { connect } from 'react-redux';
import { withTranslation, WithTranslation } from 'react-i18next';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import { collapseIcon, expandIcon, closeIcon } from '../common/icons';
import { Element, scroller } from 'react-scroll';
import { Link } from 'react-router-dom';
import { useWindowWidth } from '@react-hook/window-size';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import {
  handleChartsPairOneMinMaxMize,
  handleChartsPairTwoMinMaxMize,
} from '../actions/chartActions';
import { RootState, AppDispatch } from '../App';
import { clientReducerDefaultState } from '../reducers/clientReducer';
import { languageReducerDefaultState } from '../reducers/languageReducer';
import {
  getPossiblePeriods,
  calculatePercentageOfNumber,
} from '../utils/appUtils';
import Parcel from './Parcel';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

const useStyles = makeStyles(() => ({
  formControl: {
    width: '14.5rem',
    paddingRight: '1rem',
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: '#B8B8B8',
    },
    '& .Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: '#07a84c',
    },
  },
  formControlBigWidth: {
    margin: '0.5rem 0',
    width: '23rem',
  },
  formControlAbsolute: {
    position: 'absolute',
  },
  select: {
    fontSize: '1.4rem',
  },
  menuItem: {
    fontSize: '1.4rem',
  },
  iconButton: {
    '&:hover': {
      background: 'none',
    },
  },
}));

type renderCustomizedLabelProps = {
  name: string;
  shortName: string;
  percent: number;
};

const renderLabel = (props: renderCustomizedLabelProps) => {
  const { name, shortName, percent } = props;
  const itemName = shortName
    ? shortName.toUpperCase().substring(0, 4)
    : name.toUpperCase().substring(0, 2);
  const label = `${itemName} - ${(percent * 100).toFixed(2)}%`;
  return label;
};

const renderLegend = (props: any) => {
  const { payload } = props;

  return (
    <div className="recharts-legend-wrapper">
      <ul className="recharts-default-legend legends-list">
        {payload.map((entry: any, index: number) => {
          if (payload.length > 8 && props.iconSize === 1) {
            if (index < 8) {
              return (
                <li className="recharts-legend-item" key={index}>
                  <svg
                    className="recharts-surface piechart-legend-item-circle"
                    viewBox="0 0 32 32"
                  >
                    <path
                      fill={entry.color}
                      cx="16"
                      cy="16"
                      type="circle"
                      transform="translate(16, 16)"
                      d="M16,0A16,16,0,1,1,-16,0A16,16,0,1,1,16,0"
                    ></path>
                  </svg>
                  <span className="recharts-legend-item-text">
                    {entry.payload.shortName
                      ? entry.payload.shortName.toUpperCase().substring(0, 4)
                      : entry.payload.name.toUpperCase().substring(0, 2)}{' '}
                    - {entry.payload.name}
                  </span>
                  {index === 7 && <span>...</span>}
                </li>
              );
            } else {
              return null;
            }
          } else {
            return (
              <li className="recharts-legend-item" key={index}>
                <svg
                  className="recharts-surface piechart-legend-item-circle"
                  viewBox="0 0 32 32"
                >
                  <path
                    fill={entry.color}
                    cx="16"
                    cy="16"
                    type="circle"
                    transform="translate(16, 16)"
                    d="M16,0A16,16,0,1,1,-16,0A16,16,0,1,1,16,0"
                  ></path>
                </svg>
                <span className="recharts-legend-item-text">
                  {entry.payload.shortName
                    ? entry.payload.shortName.toUpperCase().substring(0, 4)
                    : entry.payload.name.toUpperCase().substring(0, 2)}{' '}
                  - {entry.payload.name}
                </span>
              </li>
            );
          }
        })}
      </ul>
    </div>
  );
};

type ChartItemProps = {
  title: string;
  colors: string[];
  clientState: typeof clientReducerDefaultState;
  languageState: typeof languageReducerDefaultState;
  dispatch: AppDispatch;
  chartIndex: number;
};

const ChartItem = (props: ChartItemProps & WithTranslation) => {
  const classes = useStyles();
  const { title, colors, t, clientState, languageState } = props;
  const defaultSelectedPeriodValue = t('chartsPeriodSelect.currentValue');
  const [selectedPeriodOption, setSelectedPeriodOption] = useState(
    defaultSelectedPeriodValue,
  );
  const defaultSelectedUnitsValue = t('chartsPeriodSelect.allTypesValue');
  const [selectedOptionUnit, setSelectedOptionUnit] = useState(
    defaultSelectedUnitsValue,
  );

  const defaultSelectedFarmerdashboardValue = t(
    'farmerDashboardOptions.plantProtectionValue',
  );
  const [
    selectedFarmerDashboardOption,
    setSelectedFarmerDashboardOption,
  ] = useState(defaultSelectedFarmerdashboardValue);

  const defaultSelectedSubsidiesOption = t(
    'subsidiesOptions.areaAndAnimalPaymentsValue',
  );
  const [selectedSubsidiesOption, setSelectedSubsidiesOption] = useState(
    defaultSelectedSubsidiesOption,
  );

  const economicChartUnitName = t('economicInfo.chartUnitName');
  const economicChartYAxisLabel = t('farmerDashboardOptions.economicSizeClass');

  let economicClassTicks: number[] = [];

  const [isMinimized, setMinimized] = useState(true);
  const windowWidth = useWindowWidth();
  const [parcelDialogOpen, setOpenParcelDialogOpen] = useState(false);
  const [selectedParcel, setSelectedParcel] = useState({});

  const [
    agrotechnicsPaginationPage,
    setAgrotechnicsPaginationPage,
  ] = React.useState(1);
  const handleAgrotechnicsPaginationChange = (
    event: React.ChangeEvent<unknown>,
    value: number,
  ) => {
    setAgrotechnicsPaginationPage(value);
  };

  const getTicksInClassSums = (economicTicks: number[]) => {
    let ticksInClassSums: number[] = [];

    economicTicks.map((tick) => {
      if (tick < 2000) {
        const ticksToAdd = [0, 2000];

        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });

        return ticksInClassSums;
      } else if (tick < 4000) {
        const ticksToAdd = [2000, 4000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 8000) {
        const ticksToAdd = [4000, 8000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 15000) {
        const ticksToAdd = [8000, 15000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 25000) {
        const ticksToAdd = [15000, 25000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 50000) {
        const ticksToAdd = [25000, 50000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 100000) {
        const ticksToAdd = [50000, 100000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 250000) {
        const ticksToAdd = [100000, 250000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 500000) {
        const ticksToAdd = [250000, 500000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 750000) {
        const ticksToAdd = [500000, 750000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 1000000) {
        const ticksToAdd = [750000, 1000000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 1500000) {
        const ticksToAdd = [1000000, 1500000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick < 3000000) {
        const ticksToAdd = [1500000, 3000000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else if (tick > 3000000) {
        const ticksToAdd = [3000000];
        ticksToAdd.forEach((tick) => {
          if (!ticksInClassSums.includes(tick)) {
            ticksInClassSums.push(tick);
          }
        });
        return ticksInClassSums;
      } else {
        return ticksInClassSums;
      }
    });

    return ticksInClassSums;
  };

  const getEconomicClassTicks = (data: any) => {
    const economicTicks: number[] = [];

    data.map((economicData: any) => {
      if (!economicTicks.includes(economicData.value)) {
        return economicTicks.push(economicData.value);
      } else {
        return economicTicks;
      }
    });

    const ticksInClassSums = getTicksInClassSums(economicTicks);
    return ticksInClassSums;
  };

  let pieChartData: any[] = [];
  let barAndLineChartData: any[] = [];
  let lineChartOptionsAndBarChartNames: string[] = [];
  let periods: string[] = [];
  let chartUnit: string = '';

  type TableData = {
    headers: string[];
    unitsData: any[];
    period: string;
    totals: any[];
    arableLandCultures: string[];
    arableLandCulturesHectares: string;
    totalHectares: string;
    parcels: any[];
    clientDataTableHeading: string;
    benchmarkingTableHeading: string;
  };

  let tableData: TableData = {
    headers: [],
    unitsData: [],
    totals: [],
    period: '',
    arableLandCultures: [],
    arableLandCulturesHectares: '',
    totalHectares: '',
    parcels: [],
    clientDataTableHeading: '',
    benchmarkingTableHeading: '',
  };
  let additionalTableInfo = '';
  let additionalLinkText = '';
  let additionalLinkTo = '';
  let additionalTableData;
  let pieChartDataSum = 0;

  const sortUnitsToBarChartAndLineChartFormat = (units: any) => {
    units.map((unit: any) => {
      if (!lineChartOptionsAndBarChartNames.includes(unit.name)) {
        lineChartOptionsAndBarChartNames.push(unit.name);
      }

      const index = barAndLineChartData.findIndex((data) => {
        return data.period === unit.period;
      });

      if (index !== -1) {
        barAndLineChartData[index][unit.name] = unit.value;
      } else {
        barAndLineChartData.push({
          period: unit.period,
          [unit.name]: unit.value,
        });
      }

      return barAndLineChartData;
    });

    return barAndLineChartData;
  };

  const calculatePieChartDataSum = (pieChartData: any) => {
    const pieChartDataValues = pieChartData.map((data: any) => data.value);
    let pieChartDataTotalValue = 0;

    pieChartDataValues.map((value: any) => {
      if (typeof value === 'number') {
        pieChartDataTotalValue += value;
      }
      return pieChartDataTotalValue;
    });

    return pieChartDataTotalValue;
  };

  const sortUnitsToPieChartFormat = (units: any) => {
    const pieChartData = units.filter((unit: any) => {
      return (
        unit.period === selectedPeriodOption ||
        (unit.period === periods[0] &&
          selectedPeriodOption === t('chartsPeriodSelect.currentValue'))
      );
    });

    if (pieChartData.length > 4) {
      const pieChartDataValues = pieChartData.map((data: any) => data.value);

      const highestValuesInPieChart = [...pieChartDataValues]
        .sort((a, b) => b - a)
        .slice(0, 5);

      let otherValuesSum = 0;
      pieChartDataValues.forEach((value: number) => {
        if (!highestValuesInPieChart.includes(value)) {
          otherValuesSum += value;
        }
      });

      const pieChartDataWithFiveHighestValues = pieChartData.filter(
        (data: any) => {
          return highestValuesInPieChart.includes(data.value);
        },
      );
      const otherDataName = languageState.language === 'en' ? 'Other' : 'Muu';
      const pieChartDataWithFiveHighestValuesAndOtherCombined = [
        ...pieChartDataWithFiveHighestValues,
        {
          period: pieChartDataWithFiveHighestValues[0].period,
          value: otherValuesSum,
          name: otherDataName,
        },
      ];

      return pieChartDataWithFiveHighestValuesAndOtherCombined;
    } else {
      return pieChartData;
    }
  };

  const getCurrentPeriodTableData = (tableDataToFilter: any) => {
    const tableDataWithIndex = tableDataToFilter.filter((unit: any) => {
      return (
        unit.period === selectedPeriodOption ||
        (unit.period === periods[0] &&
          selectedPeriodOption === t('chartsPeriodSelect.currentValue'))
      );
    });

    return tableDataWithIndex[0];
  };

  if (title === t('charts.parcelsTitle')) {
    const parcels =
      languageState.language === 'et'
        ? clientState.parcels.parcelsByYearEt
        : clientState.parcels.parcelsByYearEn;

    const tableDataToFilter =
      languageState.language === 'et'
        ? clientState.parcels.parcelTableDataEt
        : clientState.parcels.parcelTableDataEn;

    const additionalTableDataToFilter =
      languageState.language === 'et'
        ? clientState.parcels.parcelsLandTypeTableDataEt
        : clientState.parcels.parcelsLandTypeTableDataEn;

    if (parcels && tableDataToFilter && additionalTableDataToFilter) {
      chartUnit = clientState.parcels.unit;
      periods = getPossiblePeriods(clientState.parcels.parcelsData);
      pieChartData = sortUnitsToPieChartFormat(parcels);
      pieChartDataSum = calculatePieChartDataSum(pieChartData);
      barAndLineChartData = sortUnitsToBarChartAndLineChartFormat(parcels);
      tableData = getCurrentPeriodTableData(tableDataToFilter);
      additionalTableData = getCurrentPeriodTableData(
        additionalTableDataToFilter,
      );

      if (tableData) {
        additionalTableInfo =
          languageState.language === 'et'
            ? `Põllumaal kasvatatakse ${
                tableData.arableLandCultures.length
              } kultuuri, mis katab ${calculatePercentageOfNumber(
                tableData.arableLandCulturesHectares,
                tableData.totalHectares,
              )}% kogu põllumajandusmaast.
      `
            : `${
                tableData.arableLandCultures.length
              } crops cultivated on arable land, covering ${calculatePercentageOfNumber(
                tableData.arableLandCulturesHectares,
                tableData.totalHectares,
              )}% of utilized agricultural area.
    `;
      }
    }
  } else if (title === t('charts.animalsTitle')) {
    const animals =
      languageState.language === 'et'
        ? clientState.animals.animalsEt
        : clientState.animals.animalsEn;

    const tableDataToFilter =
      languageState.language === 'et'
        ? clientState.animals.animalsTableDataEt
        : clientState.animals.animalsTableDataEn;

    if (animals && tableDataToFilter) {
      chartUnit =
        languageState.language === 'et'
          ? clientState.animals.unitEt
          : clientState.animals.unitEn;
      periods = getPossiblePeriods(animals);
      pieChartData = sortUnitsToPieChartFormat(animals);
      pieChartDataSum = calculatePieChartDataSum(pieChartData);
      barAndLineChartData = sortUnitsToBarChartAndLineChartFormat(animals);
      tableData = getCurrentPeriodTableData(tableDataToFilter);
      additionalLinkText = `${t('livestock.linkText')}`;
      additionalLinkTo = `${t('paths.livestock')}`;
    }
  } else if (title === t('charts.subsidiesTitle')) {
    let subsidies, tableDataToFilter;

    if (
      selectedSubsidiesOption ===
      t('subsidiesOptions.areaAndAnimalPaymentsValue')
    ) {
      subsidies =
        languageState.language === 'et'
          ? clientState.subsidies.subsidiesByYearEt
          : clientState.subsidies.subsidiesByYearEn;

      tableDataToFilter =
        languageState.language === 'et'
          ? clientState.subsidies.subsidiesTableDataEt
          : clientState.subsidies.subsidiesTableDataEn;
    } else {
      subsidies = [];
      tableDataToFilter = [];
    }

    if (subsidies && tableDataToFilter) {
      chartUnit = clientState.subsidies.unit;
      periods = getPossiblePeriods(subsidies);
      pieChartData = sortUnitsToPieChartFormat(subsidies);
      pieChartDataSum = calculatePieChartDataSum(pieChartData);
      barAndLineChartData = sortUnitsToBarChartAndLineChartFormat(subsidies);
      tableData = getCurrentPeriodTableData(tableDataToFilter);
    }
  } else if (title === t('charts.farmerDashboardTitle')) {
    let units = [];
    let unitsTableData = [];

    if (
      selectedFarmerDashboardOption ===
      t('farmerDashboardOptions.plantProtectionValue')
    ) {
      units =
        languageState.language === 'et'
          ? clientState.plantChemicals.plantProtectionChemicalsEt
          : clientState.plantChemicals.plantProtectionChemicalsEn;

      unitsTableData =
        languageState.language === 'et'
          ? clientState.plantChemicals.plantProtectionChemicalsTableDataEt
          : clientState.plantChemicals.plantProtectionChemicalsTableDataEn;
    } else if (
      selectedFarmerDashboardOption ===
      t('farmerDashboardOptions.useOfFertilizersValue')
    ) {
      units =
        languageState.language === 'et'
          ? clientState.plantFertilizers.plantFertilizersEt
          : clientState.plantFertilizers.plantFertilizersEn;

      unitsTableData =
        languageState.language === 'et'
          ? clientState.plantFertilizers.plantFertilizersTableDataEt
          : clientState.plantFertilizers.plantFertilizersTableDataEn;
    } else if (
      selectedFarmerDashboardOption ===
      t('farmerDashboardOptions.livestockLoadValue')
    ) {
      chartUnit =
        languageState.language === 'et'
          ? clientState.livestockLoadData.unitEt
          : clientState.livestockLoadData.unitEn;

      units =
        languageState.language === 'et'
          ? clientState.livestockLoadData.livestockLoadDataEt
          : clientState.livestockLoadData.livestockLoadDataEn;

      unitsTableData =
        languageState.language === 'et'
          ? clientState.livestockLoadData.livestockTableDataEt
          : clientState.livestockLoadData.livestockTableDataEn;

      additionalTableInfo = t('livestock.additionalTableInfo');
    } else if (
      selectedFarmerDashboardOption ===
      t('farmerDashboardOptions.economicSizeClassValue')
    ) {
      units =
        languageState.language === 'et'
          ? clientState.economicInfo.economicInfoByYearEt
          : clientState.economicInfo.economicInfoByYearEn;
      chartUnit = clientState.economicInfo.unit;
    } else if (
      selectedFarmerDashboardOption ===
      t('farmerDashboardOptions.agrotechnicsValue')
    ) {
      units =
        languageState.language === 'et'
          ? clientState.agrotechnics.agrotechnicsTableDataEt
          : clientState.agrotechnics.agrotechnicsTableDataEn;
      unitsTableData =
        languageState.language === 'et'
          ? clientState.agrotechnics.agrotechnicsTableDataEt
          : clientState.agrotechnics.agrotechnicsTableDataEn;
    }

    if (units) {
      periods = getPossiblePeriods(units);
    }

    if (
      selectedFarmerDashboardOption ===
      t('farmerDashboardOptions.economicSizeClassValue')
    ) {
      economicClassTicks = getEconomicClassTicks(units);
      barAndLineChartData = sortUnitsToBarChartAndLineChartFormat(units);
    } else {
      if (units && unitsTableData) {
        pieChartData = sortUnitsToPieChartFormat(units);
        pieChartDataSum = calculatePieChartDataSum(pieChartData);
        barAndLineChartData = sortUnitsToBarChartAndLineChartFormat(units);
        tableData = getCurrentPeriodTableData(unitsTableData);
      }
    }
  } else {
    pieChartData = [];
    barAndLineChartData = [];
  }

  barAndLineChartData.sort((a, b) => {
    const periodA = parseInt(a.period);
    const periodB = parseInt(b.period);

    if (periodA > periodB) {
      return 1;
    } else if (periodA < periodB) {
      return -1;
    } else {
      return 0;
    }
  });

  const chartsDataFound =
    pieChartData.length !== 0 && barAndLineChartData.length !== 0;

  const handlePeriodChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const optionValue = e.target.value as string;
    setSelectedPeriodOption(optionValue);
  };

  const optionUnitChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const optionValue = e.target.value as string;
    setSelectedOptionUnit(optionValue);
  };

  const handleFarmerDashboardOptionChange = (
    e: React.ChangeEvent<{ value: unknown }>,
  ) => {
    const optionValue = e.target.value as string;
    setSelectedPeriodOption(defaultSelectedPeriodValue);
    setSelectedFarmerDashboardOption(optionValue);
  };

  const handleSubsidiesOptionChange = (
    e: React.ChangeEvent<{ value: unknown }>,
  ) => {
    const optionValue = e.target.value as string;
    setSelectedSubsidiesOption(optionValue);
  };

  const handleMinMaximize = async () => {
    const previousValue = isMinimized;
    await setMinimized(!previousValue);

    if (props.chartIndex < 2) {
      props.dispatch(
        handleChartsPairOneMinMaxMize({
          index: props.chartIndex,
          minimized: !previousValue,
        }),
      );
    } else {
      props.dispatch(
        handleChartsPairTwoMinMaxMize({
          index: props.chartIndex,
          minimized: !previousValue,
        }),
      );
    }

    if (isMinimized) {
      scroller.scrollTo(title, {
        duration: 500,
        delay: 200,
        smooth: 'easeInOutQuart',
        isDynamic: true,
        offset: -200,
      });
    }
  };

  const handleMaximize = async () => {
    await setMinimized(false);

    if (props.chartIndex < 2) {
      props.dispatch(
        handleChartsPairOneMinMaxMize({
          index: props.chartIndex,
          minimized: false,
        }),
      );
    } else {
      props.dispatch(
        handleChartsPairTwoMinMaxMize({
          index: props.chartIndex,
          minimized: false,
        }),
      );
    }

    if (isMinimized) {
      scroller.scrollTo(title, {
        duration: 500,
        delay: 200,
        smooth: 'easeInOutQuart',
        isDynamic: true,
        offset: -200,
      });
    }
  };

  const handleParcelDialogOpen = (e: React.MouseEvent<HTMLHeadingElement>) => {
    const rcpKey = e.currentTarget.id;
    const parcel = clientState.parcels.parcelsData.find((parcelData: any) => {
      return parcelData.rcpKey === rcpKey;
    });

    setSelectedParcel(parcel);
    setOpenParcelDialogOpen(true);
  };

  const handleParcelDialogClose = () => {
    setSelectedParcel({});
    setOpenParcelDialogOpen(false);
  };

  const convertEconomicTotalToClassNumber = (number: any) => {
    if (number < 2000) {
      return '1';
    } else if (number < 4000) {
      return '2';
    } else if (number < 8000) {
      return '3';
    } else if (number < 15000) {
      return '4';
    } else if (number < 25000) {
      return '5';
    } else if (number < 50000) {
      return '6';
    } else if (number < 100000) {
      return '7';
    } else if (number < 250000) {
      return '8';
    } else if (number < 500000) {
      return '9';
    } else if (number < 750000) {
      return '10';
    } else if (number < 1000000) {
      return '11';
    } else if (number < 1500000) {
      return '12';
    } else if (number < 3000000) {
      return '13';
    } else {
      return '14';
    }
  };

  const getAgroTechnicsPageBasedOnIndex = (index: number) => {
    const pageSize = 5;
    return Math.ceil(++index / pageSize);
  };

  const isOnCurrentAgrotechnicsPagination = (index: number) => {
    const agrotechnicsPage = getAgroTechnicsPageBasedOnIndex(index);

    if (agrotechnicsPage === agrotechnicsPaginationPage) {
      return true;
    }
  };

  const getAgroTechnicsPageCount = () => {
    return Math.floor((tableData.parcels.length + 5 - 1) / 5);
  };

  return (
    <div
      className={
        isMinimized
          ? 'chart-container'
          : 'chart-container chart-container--maximized'
      }
    >
      <div
        className={
          selectedPeriodOption !== t('chartsPeriodSelect.allYearsValue') &&
          isMinimized &&
          windowWidth > 1040
            ? `chart-container__items-box`
            : `chart-container__items-box chart-container__items-box--small-margin`
        }
      >
        <Element name={title}>
          <div className="chart-container-title-box">
            <h1 className="chart-container-title-box__heading">{title}</h1>

            {title === t('charts.farmerDashboardTitle') && (
              <FormControl
                variant="outlined"
                className={`${classes.formControl} ${
                  classes.formControlBigWidth
                } ${
                  selectedPeriodOption !==
                    t('chartsPeriodSelect.allYearsValue') &&
                  isMinimized &&
                  windowWidth > 1040 &&
                  selectedFarmerDashboardOption !==
                    t('farmerDashboardOptions.economicSizeClassValue') &&
                  classes.formControlAbsolute
                }`}
              >
                <Select
                  value={selectedFarmerDashboardOption}
                  onChange={handleFarmerDashboardOptionChange}
                  className={classes.select}
                >
                  <MenuItem
                    value={defaultSelectedFarmerdashboardValue}
                    className={classes.menuItem}
                  >
                    {t('farmerDashboardOptions.plantProtection')}
                  </MenuItem>
                  <MenuItem
                    value={t('farmerDashboardOptions.useOfFertilizersValue')}
                    className={classes.menuItem}
                  >
                    {t('farmerDashboardOptions.useOfFertilizers')}
                  </MenuItem>

                  <MenuItem
                    value={t('farmerDashboardOptions.agrotechnicsValue')}
                    className={classes.menuItem}
                  >
                    {t('farmerDashboardOptions.agrotechnics')}
                  </MenuItem>

                  <MenuItem
                    value={t('farmerDashboardOptions.livestockLoadValue')}
                    className={classes.menuItem}
                  >
                    {t('farmerDashboardOptions.livestockLoad')}
                  </MenuItem>

                  <MenuItem
                    value={t('farmerDashboardOptions.economicSizeClassValue')}
                    className={classes.menuItem}
                  >
                    {t('farmerDashboardOptions.economicSizeClass')}
                  </MenuItem>
                </Select>
              </FormControl>
            )}

            {title === t('charts.subsidiesTitle') && (
              <FormControl
                variant="outlined"
                className={`${classes.formControl} ${
                  classes.formControlBigWidth
                } ${
                  selectedPeriodOption !==
                    t('chartsPeriodSelect.allYearsValue') &&
                  isMinimized &&
                  windowWidth > 1040 &&
                  classes.formControlAbsolute
                }`}
              >
                <Select
                  value={selectedSubsidiesOption}
                  onChange={handleSubsidiesOptionChange}
                  className={classes.select}
                >
                  <MenuItem
                    value={defaultSelectedSubsidiesOption}
                    className={classes.menuItem}
                  >
                    {t('subsidiesOptions.areaAndAnimalPayments')}
                  </MenuItem>
                  <MenuItem
                    value={t('subsidesOptions.developmentAidValue')}
                    className={classes.menuItem}
                  >
                    {t('subsidiesOptions.developmentAid')}
                  </MenuItem>
                </Select>
              </FormControl>
            )}
          </div>
        </Element>

        <div className="select-min-max-button-container">
          <div className="select-menus-container">
            {selectedPeriodOption !== t('chartsPeriodSelect.allYearsValue') &&
              isMinimized &&
              windowWidth > 1040 &&
              selectedFarmerDashboardOption !==
                t('farmerDashboardOptions.economicSizeClassValue') && (
                <FormControl
                  variant="outlined"
                  className={`${classes.formControl}`}
                >
                  <Select
                    value={selectedPeriodOption}
                    onChange={handlePeriodChange}
                    className={classes.select}
                  >
                    <MenuItem
                      value={t('chartsPeriodSelect.currentValue')}
                      className={classes.menuItem}
                    >
                      {t('chartsPeriodSelect.current')}
                    </MenuItem>

                    {selectedFarmerDashboardOption !==
                      t('farmerDashboardOptions.agrotechnicsValue') && (
                      <MenuItem
                        value={t('chartsPeriodSelect.allYearsValue')}
                        className={classes.menuItem}
                      >
                        {t('chartsPeriodSelect.allYears')}
                      </MenuItem>
                    )}

                    {periods.map((period, index) => {
                      if (index !== 0) {
                        return (
                          <MenuItem
                            value={period}
                            className={classes.menuItem}
                            key={index}
                          >
                            {period}
                          </MenuItem>
                        );
                      } else {
                        return null;
                      }
                    })}
                  </Select>
                </FormControl>
              )}
          </div>

          <div className="collapse-icon-container">
            <IconButton
              onClick={handleMinMaximize}
              disableRipple
              className={classes.iconButton}
            >
              <img
                src={isMinimized ? expandIcon : collapseIcon}
                alt={isMinimized ? 'expand' : 'collapse'}
              />
            </IconButton>
          </div>
        </div>
      </div>

      <div className="select-menus-container">
        {(selectedPeriodOption === t('chartsPeriodSelect.allYearsValue') ||
          windowWidth <= 1040 ||
          !isMinimized) &&
          selectedFarmerDashboardOption !==
            t('farmerDashboardOptions.economicSizeClassValue') && (
            <FormControl
              variant="outlined"
              className={`${classes.formControl}`}
            >
              <Select
                value={selectedPeriodOption}
                onChange={handlePeriodChange}
                className={classes.select}
              >
                <MenuItem
                  value={t('chartsPeriodSelect.currentValue')}
                  className={classes.menuItem}
                >
                  {t('chartsPeriodSelect.current')}
                </MenuItem>

                {selectedFarmerDashboardOption !==
                  t('farmerDashboardOptions.agrotechnicsValue') && (
                  <MenuItem
                    value={t('chartsPeriodSelect.allYearsValue')}
                    className={classes.menuItem}
                  >
                    {t('chartsPeriodSelect.allYears')}
                  </MenuItem>
                )}

                {periods.map((period, index) => {
                  if (index !== 0) {
                    return (
                      <MenuItem
                        value={period}
                        className={classes.menuItem}
                        key={index}
                      >
                        {period}
                      </MenuItem>
                    );
                  } else {
                    return null;
                  }
                })}
              </Select>
            </FormControl>
          )}

        {selectedPeriodOption === t('chartsPeriodSelect.allYearsValue') && (
          <FormControl variant="outlined" className={classes.formControl}>
            <Select
              value={selectedOptionUnit}
              onChange={optionUnitChange}
              className={classes.select}
            >
              <MenuItem
                value={t('chartsPeriodSelect.allTypesValue')}
                className={classes.menuItem}
              >
                {t('chartsPeriodSelect.allTypes')}
              </MenuItem>

              {lineChartOptionsAndBarChartNames.map(
                (name: string, index: number) => {
                  return (
                    <MenuItem
                      value={name}
                      className={classes.menuItem}
                      key={index}
                    >
                      {name}
                    </MenuItem>
                  );
                },
              )}
            </Select>
          </FormControl>
        )}
      </div>

      <Dialog
        open={parcelDialogOpen}
        onClose={handleParcelDialogClose}
        fullWidth
      >
        <DialogContent>
          <div className="parcel-close-icon-container">
            <IconButton
              onClick={handleParcelDialogClose}
              disableRipple
              className={classes.iconButton}
            >
              <img src={closeIcon} alt="close" />
            </IconButton>
          </div>
          <Parcel {...selectedParcel} />
        </DialogContent>
      </Dialog>

      {selectedPeriodOption !== t('chartsPeriodSelect.allYearsValue') &&
      selectedFarmerDashboardOption !==
        t('farmerDashboardOptions.economicSizeClassValue') &&
      selectedFarmerDashboardOption !==
        t('farmerDashboardOptions.agrotechnicsValue') ? (
        <div className="pie-container">
          <div
            className={
              isMinimized && windowWidth >= 1040
                ? 'pie-container-items'
                : 'pie-container-items pie-container-items--maximized'
            }
          >
            <ResponsiveContainer minWidth="100%" height="100%" debounce={10}>
              {chartsDataFound && pieChartDataSum !== 0 ? (
                <PieChart>
                  <Tooltip
                    formatter={(value: any) => {
                      return `${value} ${chartUnit}`;
                    }}
                    labelFormatter={(value: any) => {
                      if (value !== 0) {
                        return value;
                      } else {
                        return '';
                      }
                    }}
                  />
                  <Pie
                    data={pieChartData}
                    dataKey="value"
                    cx="50%"
                    cy="40%"
                    labelLine={false}
                    stroke="none"
                    label={renderLabel}
                    outerRadius={120}
                    isAnimationActive={false}
                    minAngle={1.5}
                  >
                    {pieChartData.map((entry: any, index: any) => (
                      <Cell
                        key={`cell-${index}`}
                        fill={colors[index % colors.length]}
                      />
                    ))}
                  </Pie>

                  <Legend
                    content={renderLegend}
                    verticalAlign={'bottom'}
                    align="left"
                    iconSize={!isMinimized || windowWidth <= 1040 ? 2 : 1}
                  />
                </PieChart>
              ) : (
                <div className="data-not-found-container">
                  <p className="data-not-found-container__text">
                    {t('charts.dataNotFoundError')}
                  </p>
                </div>
              )}
            </ResponsiveContainer>
          </div>
          {(!isMinimized || windowWidth <= 1040) && (
            <div className="piechart-table">
              {tableData && (
                <div className="tableData-container">
                  <h1 className="tableData-container__heading">
                    {tableData.clientDataTableHeading}
                  </h1>
                  <table className="table table--center">
                    <tbody>
                      <tr
                        className="table__row"
                        style={{ color: '#ffffff', backgroundColor: colors[0] }}
                      >
                        {tableData.headers.map((header: any, index: number) => {
                          return (
                            <th className="table__header-cell" key={index}>
                              {header}
                            </th>
                          );
                        })}
                      </tr>
                      {tableData.unitsData.map(
                        (unitData: any, index: number) => {
                          return (
                            <tr className="table__row" key={index}>
                              {unitData.map((data: any, index: number) => {
                                return (
                                  <td className="table__data-cell" key={index}>
                                    {data}
                                  </td>
                                );
                              })}
                            </tr>
                          );
                        },
                      )}

                      <tr className="table__row table__row--bold">
                        {tableData.totals.map((total: any, index: number) => {
                          return (
                            <td className="table__data-cell" key={index}>
                              {total}
                            </td>
                          );
                        })}
                      </tr>
                    </tbody>
                  </table>
                </div>
              )}
              {additionalTableData && (
                <div className="tableData-container">
                  <h1 className="tableData-container__heading">
                    {tableData.benchmarkingTableHeading}
                  </h1>

                  <table className="table table--center">
                    <tbody>
                      <tr
                        className="table__row"
                        style={{ color: '#ffffff', backgroundColor: colors[0] }}
                      >
                        {additionalTableData.headers.map(
                          (header: any, index: number) => {
                            return (
                              <th className="table__header-cell" key={index}>
                                {header}
                              </th>
                            );
                          },
                        )}
                      </tr>
                      {additionalTableData.unitsData.map(
                        (unitData: any, index: number) => {
                          return (
                            <tr className="table__row" key={index}>
                              {unitData.map((data: any, index: number) => {
                                return (
                                  <td className="table__data-cell" key={index}>
                                    {data}
                                  </td>
                                );
                              })}
                            </tr>
                          );
                        },
                      )}

                      <tr className="table__row table__row--bold">
                        {additionalTableData.totals.map(
                          (total: any, index: number) => {
                            return (
                              <td className="table__data-cell" key={index}>
                                {total}
                              </td>
                            );
                          },
                        )}
                      </tr>
                    </tbody>
                  </table>
                </div>
              )}
              {pieChartData.map((entry: any, index) => {
                if (entry.cultures) {
                  const cultureDataKeys = Object.keys(entry.cultures).sort(
                    (a, b) => {
                      return entry.cultures[b] - entry.cultures[a];
                    },
                  );

                  const locatedSelectedPeriod =
                    selectedPeriodOption ===
                    t('chartsPeriodSelect.currentValue')
                      ? t('chartsPeriodSelect.current').toLowerCase()
                      : selectedPeriodOption;

                  return (
                    <div className="cultures-container" key={index}>
                      <h1 className="cultures-container__heading">
                        <Link
                          className="cultures-container__link"
                          to={`${t('paths.client')}/${clientState.id}${t(
                            'paths.parcels',
                          )}?${t(
                            'parcelQueryParams.period',
                          )}=${locatedSelectedPeriod}&${t(
                            'parcelQueryParams.type',
                          )}=${entry.name}`}
                          target="_blank"
                        >
                          {entry.name}
                        </Link>
                      </h1>
                      <table className="botanical-names-table">
                        {cultureDataKeys.map((key: any, index: number) => (
                          <tbody key={index}>
                            <tr className="botanical-names-table__row">
                              <td className="botanical-names-table__cell botanical-names-table__cell--first">
                                {key}:
                              </td>

                              <td className="botanical-names-table__cell">
                                {entry.cultures[key]} {entry.unit} (
                                {calculatePercentageOfNumber(
                                  entry.cultures[key],
                                  tableData.totals[1],
                                )}{' '}
                                %)
                              </td>
                            </tr>
                          </tbody>
                        ))}
                      </table>
                    </div>
                  );
                } else {
                  return null;
                }
              })}
              {additionalTableInfo && (
                <p className="piechart-table__paragraph">
                  {additionalTableInfo}
                </p>
              )}
              {additionalLinkText && (
                <div>
                  <Link
                    className="tooltip-link"
                    style={{ color: colors[0] }}
                    to={additionalLinkTo}
                    target="_blank"
                  >
                    {additionalLinkText}
                  </Link>
                </div>
              )}
            </div>
          )}
        </div>
      ) : selectedPeriodOption === t('chartsPeriodSelect.allYearsValue') &&
        selectedOptionUnit === t('chartsPeriodSelect.allTypesValue') &&
        selectedFarmerDashboardOption !==
          t('farmerDashboardOptions.economicSizeClassValue') &&
        selectedFarmerDashboardOption !==
          t('farmerDashboardOptions.agrotechnicsValue') ? (
        <ResponsiveContainer width="100%" height={isMinimized ? 350 : 450}>
          <BarChart data={barAndLineChartData}>
            <CartesianGrid strokeDasharray="33" />
            <XAxis dataKey="period" fontSize={'1.4rem'} />
            <YAxis fontSize={'1.4rem'} />
            <Tooltip
              cursor={{ fill: 'rgba(206, 206, 206, 0.2)' }}
              formatter={(value: any) => {
                return `${value} ${chartUnit}`;
              }}
            />

            {(!isMinimized || windowWidth <= 1040) && <Legend />}
            {lineChartOptionsAndBarChartNames.map(
              (name: string, index: number) => {
                return <Bar dataKey={name} fill={colors[index]} key={index} />;
              },
            )}
          </BarChart>
        </ResponsiveContainer>
      ) : selectedFarmerDashboardOption ===
        t('farmerDashboardOptions.economicSizeClassValue') ? (
        <div className="economic-size-container">
          {!isMinimized && (
            <div>
              <h1 className="economic-size-container__heading">
                {t('economicSizeClasses.title')}
              </h1>
              <ul className="economic-size-container__list">
                <li className="economic-size-container__item">1 = 0 - 1999</li>
                <li className="economic-size-container__item">
                  2 = 2000 - 3999
                </li>
                <li className="economic-size-container__item">
                  3 = 4000 - 7999
                </li>
                <li className="economic-size-container__item">
                  4 = 8000 - 14 999
                </li>
                <li className="economic-size-container__item">
                  5 = 15000 - 24 999
                </li>
                <li className="economic-size-container__item">
                  6 = 25000 - 49 999
                </li>
                <li className="economic-size-container__item">
                  7 = 50000 - 99 999
                </li>
                <li className="economic-size-container__item">
                  8 = 100 000 - 249 999
                </li>
                <li className="economic-size-container__item">
                  9 = 250 000 - 499 999
                </li>
                <li className="economic-size-container__item">
                  10 = 500 000 - 749 999
                </li>
                <li className="economic-size-container__item">
                  11 = 750 000 - 999 999
                </li>
                <li className="economic-size-container__item">
                  12 = 1 000 000 - 1 499 999
                </li>
                <li className="economic-size-container__item">
                  13 = 1 500 000 - 2 999 999
                </li>
                <li className="economic-size-container__item">
                  14 = {'>= 3 000 000'}
                </li>
              </ul>
            </div>
          )}

          <ResponsiveContainer width="100%" height={isMinimized ? 350 : 450}>
            <LineChart
              width={730}
              height={250}
              data={barAndLineChartData}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray="33" />
              <XAxis dataKey="period" fontSize={'1.4rem'} />

              <YAxis
                label={
                  <Text
                    x={0}
                    y={0}
                    dx={40}
                    dy={170}
                    offset={0}
                    angle={-90}
                    fontSize={12}
                  >
                    {economicChartYAxisLabel}
                  </Text>
                }
                ticks={economicClassTicks}
                interval={0}
                tickFormatter={convertEconomicTotalToClassNumber}
                fontSize={'1.4rem'}
              />
              <Tooltip
                formatter={(value: any) => {
                  return `${value} ${chartUnit}`;
                }}
              />
              <Legend />

              <Line
                dataKey={
                  selectedFarmerDashboardOption !==
                  t('farmerDashboardOptions.economicSizeClassValue')
                    ? selectedOptionUnit
                    : economicChartUnitName
                }
                stroke={colors[0]}
              />
            </LineChart>
          </ResponsiveContainer>
        </div>
      ) : selectedFarmerDashboardOption !==
        t('farmerDashboardOptions.agrotechnicsValue') ? (
        <div className="economic-size-container">
          <ResponsiveContainer width="100%" height={isMinimized ? 350 : 450}>
            <LineChart
              width={730}
              height={250}
              data={barAndLineChartData}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
            >
              <CartesianGrid strokeDasharray="33" />
              <XAxis dataKey="period" fontSize={'1.4rem'} />
              <YAxis
                ticks={
                  selectedFarmerDashboardOption ===
                  t('farmerDashboardOptions.economicSizeClassValue')
                    ? economicClassTicks
                    : undefined
                }
                interval={0}
                tickFormatter={
                  selectedFarmerDashboardOption ===
                  t('farmerDashboardOptions.economicSizeClassValue')
                    ? convertEconomicTotalToClassNumber
                    : undefined
                }
                fontSize={'1.4rem'}
              />
              <Tooltip
                formatter={(value: any) => {
                  return `${value} ${chartUnit}`;
                }}
              />
              <Legend />

              <Line
                dataKey={
                  selectedFarmerDashboardOption !==
                  t('farmerDashboardOptions.economicSizeClassValue')
                    ? selectedOptionUnit
                    : economicChartUnitName
                }
                stroke={colors[0]}
              />
            </LineChart>
          </ResponsiveContainer>
        </div>
      ) : selectedFarmerDashboardOption ===
          t('farmerDashboardOptions.agrotechnicsValue') && tableData ? (
        <div className="agrotechnics-container">
          {tableData && !isMinimized && (
            <div className="agrotechnics-pagination">
              <Stack spacing={2}>
                <Pagination
                  count={getAgroTechnicsPageCount()}
                  page={agrotechnicsPaginationPage}
                  onChange={handleAgrotechnicsPaginationChange}
                  size={'medium'}
                  shape={'rounded'}
                  variant={'outlined'}
                />
              </Stack>
            </div>
          )}

          {tableData &&
            tableData.parcels.map((parcel: any, index: number) => {
              if (
                (index < 1 && isOnCurrentAgrotechnicsPagination(index)) ||
                (!isMinimized && isOnCurrentAgrotechnicsPagination(index)) ||
                (windowWidth <= 1040 &&
                  isOnCurrentAgrotechnicsPagination(index))
              ) {
                return (
                  <div className="agrotechnics" key={index}>
                    <h1
                      className="agrotechnics__title"
                      id={parcel.rcpKey}
                      onClick={handleParcelDialogOpen}
                    >
                      {t('agrotechnics.parcel')} {parcel.rcpKey}
                    </h1>
                    <table className="table table--center">
                      <tbody>
                        <tr
                          className="table__row"
                          style={{
                            color: '#ffffff',
                            backgroundColor: colors[0],
                          }}
                        >
                          {tableData.headers.map(
                            (header: any, index: number) => {
                              return (
                                <th className="table__header-cell" key={index}>
                                  {header}
                                </th>
                              );
                            },
                          )}
                        </tr>

                        {parcel.unitsData.map(
                          (unitData: any, index: number) => {
                            if (
                              index < 6 ||
                              !isMinimized ||
                              windowWidth <= 1040
                            ) {
                              return (
                                <tr className="table__row" key={index}>
                                  {unitData.map((data: any, index: number) => {
                                    return (
                                      <td
                                        className="table__data-cell"
                                        key={index}
                                      >
                                        {data}
                                      </td>
                                    );
                                  })}
                                </tr>
                              );
                            } else {
                              return null;
                            }
                          },
                        )}
                      </tbody>
                    </table>
                    {isMinimized && windowWidth > 1040 && (
                      <p
                        className="agrotechnics__link"
                        onClick={handleMaximize}
                        style={{ color: colors[0] }}
                      >
                        {t('agrotechnics.buttonText')}
                      </p>
                    )}
                  </div>
                );
              } else {
                return null;
              }
            })}
        </div>
      ) : (
        <div className="not-found-container-items">
          <div className="data-not-found-container">
            <p className="data-not-found-container__text">
              {t('charts.dataNotFoundError')}
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  clientState: state.clientReducer,
  languageState: state.languageReducer,
});

export default connect(mapStateToProps)(withTranslation()(ChartItem));
