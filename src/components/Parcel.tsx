/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { connect } from 'react-redux';
import { withTranslation, WithTranslation } from 'react-i18next';
import { RootState } from '../App';
import Map from './Map';

const Parcel = (props: any & WithTranslation) => {
  const {
    rcpKey,
    ablKey,
    cpgCoordinates,
    crpNameEt,
    crpNameEn,
    languageState,
    hectares,
    parcelYield,
    t,
  } = props;
  const crpName = languageState.language === 'et' ? crpNameEt : crpNameEn;

  return (
    <div className="parcel-container">
      <h1 className="parcel-container__heading">
        {t('parcel.number')}: {rcpKey}
      </h1>
      <ul className="parcel-container-list">
        <li className="parcel-container-list__item">
          {t('parcel.referenceNumber')}: {ablKey}
        </li>
        <li className="parcel-container-list__item">
          {t('parcel.area')}: {hectares}
        </li>
        <li className="parcel-container-list__item">
          {t('parcel.crop')}: {crpName}
        </li>
        <li className="parcel-container-list__item">
          {t('parcel.plannedYield')}:{' '}
          {parcelYield ? parcelYield.cryPlannedAmount : '-'}{' '}
          {parcelYield && languageState.language === 'et'
            ? parcelYield.unitNameEt
            : parcelYield && languageState.language === 'en'
            ? parcelYield.unitNameEn
            : ''}
        </li>
        <li className="parcel-container-list__item">
          {t('parcel.actualYield')}:{' '}
          {parcelYield ? parcelYield.cryActualAmount : '-'}{' '}
          {parcelYield && languageState.language === 'et'
            ? parcelYield.unitNameEt
            : parcelYield && languageState.language === 'en'
            ? parcelYield.unitNameEn
            : ''}
        </li>
      </ul>
      <Map singleParcelCoordinates={cpgCoordinates} />
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  languageState: state.languageReducer,
});

export default connect(mapStateToProps)(withTranslation()(Parcel));
