/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import React, { ChangeEvent } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { withTranslation, WithTranslation } from 'react-i18next';
import TextField from '../common/inputs/TextField';
import Autocomplete from '../common/inputs/AutoComplete';
import Button from '../common/buttons/Button';
import { Subject, combineLatest, interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { CircularProgress } from '@material-ui/core';
import { AppDispatch, RootState } from '../App';
import { setClient } from '../actions/clientActions';
import { handleResetChartsToDefaultState } from '../actions/chartActions';
import { clientReducerDefaultState } from '../reducers/clientReducer';

interface Farmer {
  fadPersonalId: string;
  fadName: string;
}

interface Params {
  inputProps: any;
}

type ClientSearchProps = WithTranslation &
  RouteComponentProps & {
    dispatch: AppDispatch;
    clientState: typeof clientReducerDefaultState;
  };

interface Farmers extends Array<Farmer> {}

interface ClientSearchState {
  farmers: Farmers;
  searchClientLoading: boolean;
  clientName: string[] | string;
  clientId: string[] | string;
  errorText: string;
}

class ClientSearch extends React.Component<
  ClientSearchProps,
  ClientSearchState
> {
  state = {
    farmers: [],
    searchClientLoading: false,
    clientName: '',
    clientId: '',
    errorText: '',
  };

  handleClientNameChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const clientName = e.target.value;
    this.setState({ clientName });
    this.clearValidationError();

    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/farmer?name=${e.target.value}`,
    );

    if (response.status === 200) {
      const responseJSON = await response.json();
      this.setState({ farmers: responseJSON });
    } else {
      this.setState({ farmers: [] });
    }
  };

  handleClientIdChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const clientId = e.target.value;
    this.setState({ clientId });
    this.clearValidationError();

    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/farmer?clientId=${e.target.value}`,
    );

    if (response.status === 200) {
      const responseJSON = await response.json();
      this.setState({ farmers: responseJSON });
    } else {
      this.setState({ farmers: [] });
    }
  };

  validateInputs = () => {
    if (!this.state.clientName && !this.state.clientId) {
      this.setValidationError('clientSearch.fieldsEmptyError');
    }
  };

  getURLToFetch = (): string => {
    if (this.state.clientName && this.state.clientId) {
      return `${process.env.REACT_APP_API_URL}/farmer?name=${this.state.clientName}&clientId=${this.state.clientId}&limit=1`;
    } else if (this.state.clientName) {
      return `${process.env.REACT_APP_API_URL}/farmer?name=${this.state.clientName}&limit=1`;
    } else if (this.state.clientId) {
      return `${process.env.REACT_APP_API_URL}/farmer?clientId=${this.state.clientId}&limit=1`;
    } else {
      return '';
    }
  };

  handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const $intervalAfterClientSearch = interval(1000);
    const $searchClientObs = new Subject();

    this.setState({ searchClientLoading: true });
    combineLatest([$intervalAfterClientSearch, $searchClientObs])
      .pipe(take(1))
      .subscribe(() => {
        this.props.history.push(
          `${this.props.t('paths.client')}/${this.props.clientState.id}`,
        );
      });

    await this.validateInputs();

    if (this.state.errorText === '') {
      const urlToFetch = this.getURLToFetch();
      try {
        const response = await fetch(urlToFetch);
        if (response.status === 200) {
          const responseJSON = await response.json();
          this.props.dispatch(handleResetChartsToDefaultState());
          this.props.dispatch(
            setClient({
              ...responseJSON,
            }),
          );
          $searchClientObs.next(null);
          $searchClientObs.complete();
        } else {
          this.setValidationError('clientSearch.notFoundError');
        }
      } catch (e) {
        this.setValidationError('clientSearch.notFoundError');
      }
    } else {
      this.setState({ searchClientLoading: false });
      $searchClientObs.complete();
    }
  };

  setValidationError = (errorText: string) => {
    this.setState({ errorText, searchClientLoading: false });
  };

  clearValidationError = () => {
    if (this.state.errorText !== '') {
      this.setState({ errorText: '' });
    }
  };

  handleAutoCompleteClientNameChange = (
    e: ChangeEvent<{}>,
    value: string[] | string | null,
  ) => {
    const clientName = value;
    if (clientName) {
      this.setState({ clientName });
    } else {
      this.setState({ clientName: '' });
    }
  };

  handleAutoCompleteClientIdChange = (
    e: ChangeEvent<{}>,
    value: string[] | string | null,
  ) => {
    const clientId = value;
    if (clientId) {
      this.setState({ clientId });
    }
  };

  handleResetFarmerResults = () => {
    this.setState({ farmers: [] });
  };

  render() {
    const { t } = this.props;
    return (
      <div className="client-search-container">
        <h1 className="client-search-container__heading">
          {t('clientSearch.selection')}
        </h1>
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form__input-container">
            <Autocomplete
              onChange={this.handleAutoCompleteClientNameChange}
              onInputChange={this.handleAutoCompleteClientNameChange}
              options={this.state.farmers.map(
                (farmer: Farmer) => farmer.fadName,
              )}
              renderInput={(params: Params) => (
                <TextField
                  error={this.state.errorText !== ''}
                  params={params}
                  onChange={this.handleClientNameChange}
                  onBlur={this.handleResetFarmerResults}
                  label={t('clientSearch.name')}
                  variant="outlined"
                />
              )}
            />
          </div>

          <div className="form__input-container">
            <Autocomplete
              onChange={this.handleAutoCompleteClientIdChange}
              onInputChange={this.handleAutoCompleteClientIdChange}
              options={this.state.farmers.map(
                (farmer: Farmer) => farmer.fadPersonalId,
              )}
              renderInput={(params: Params) => (
                <TextField
                  error={this.state.errorText !== ''}
                  helperText={t(`${this.state.errorText}`)}
                  params={params}
                  onChange={this.handleClientIdChange}
                  onBlur={this.handleResetFarmerResults}
                  label={t('clientSearch.code')}
                  variant="outlined"
                />
              )}
            />
          </div>
          <div className="form__button-container">
            {!this.state.searchClientLoading && (
              <Button type="submit" variant="contained">
                {t('clientSearch.button')}
              </Button>
            )}

            {this.state.searchClientLoading && (
              <div className="form__circ-progress">
                <CircularProgress size={35} color={'inherit'} />
              </div>
            )}
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  clientState: state.clientReducer,
});

export default connect(mapStateToProps)(withTranslation()(ClientSearch));
