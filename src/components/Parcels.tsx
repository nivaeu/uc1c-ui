/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import React from 'react';
import { connect } from 'react-redux';
import { withTranslation, WithTranslation } from 'react-i18next';
import { RouteComponentProps } from 'react-router';
import { RootState, AppDispatch } from '../App';
import Parcel from './Parcel';
import { getPossiblePeriods } from '../utils/appUtils';
import { clientReducerDefaultState } from '../reducers/clientReducer';
import { languageReducerDefaultState } from '../reducers/languageReducer';
import { setParcels } from '../actions/clientActions';
import { calculatePatternIndex } from '../utils/appUtils';
import Map from './Map';

type MultiParcelCoordinates = {
  coordinates: Array<{
    lng: number;
    lat: number;
  }>;
};

type ParcelsProps = RouteComponentProps &
  WithTranslation & {
    dispatch: AppDispatch;
    clientState: typeof clientReducerDefaultState;
    languageState: typeof languageReducerDefaultState;
  };

type ParcelsState = {
  clientNotFoundError: string;
};

class Parcels extends React.Component<ParcelsProps, ParcelsState> {
  state = {
    clientNotFoundError: '',
    coordinates: [],
  };

  fetchClientDataIfNeeded = async () => {
    if (!this.props.clientState.name) {
      const pathname = this.props.location.pathname;
      const secondSlashIndex = calculatePatternIndex(pathname, '/', 2);
      const thirdSlashIndex = calculatePatternIndex(pathname, '/', 3);
      const clientId = pathname.substring(
        secondSlashIndex + 1,
        thirdSlashIndex,
      );

      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/farmer?farmerId=${clientId}&limit=1`,
      );

      if (response.status === 200) {
        const responseJSON = await response.json();
        const { parcels } = responseJSON;
        this.props.dispatch(
          setParcels({
            parcels,
          }),
        );
      } else {
        this.setState({
          clientNotFoundError: 'clientView.clientNotFoundError',
        });
      }
    }
  };

  componentDidMount = () => {
    this.fetchClientDataIfNeeded();
  };

  render() {
    const { t, clientState, languageState, location } = this.props;
    const searchParams = location.search;
    const locatedPeriod = new URLSearchParams(searchParams).get(
      t('parcelQueryParams.period'),
    );
    const period =
      locatedPeriod === t('chartsPeriodSelect.current').toLowerCase()
        ? t('chartsPeriodSelect.currentValue').toLowerCase()
        : locatedPeriod;
    const type = new URLSearchParams(searchParams).get(
      t('parcelQueryParams.type'),
    );
    let parcels: any[] = [];
    let multiParcelCoordinates: MultiParcelCoordinates[][] = [];

    if (clientState.parcels.parcelsData) {
      const periods = getPossiblePeriods(clientState.parcels.parcelsData);
      const clanName =
        languageState.language === 'et' ? 'clanNameEt' : 'clanNameEn';

      parcels = clientState.parcels.parcelsData.filter((parcel: any) => {
        return type
          ? parcel.period === period ||
              (parcel.period === periods[0] &&
                period ===
                  this.props
                    .t('chartsPeriodSelect.currentValue')
                    .toLowerCase() &&
                type === parcel[clanName])
          : parcel.period === period ||
              (parcel.period === periods[0] &&
                period ===
                  this.props
                    .t('chartsPeriodSelect.currentValue')
                    .toLowerCase());
      });

      parcels.map((parcel: any, index: number) => {
        return multiParcelCoordinates.push(parcel.cpgCoordinates);
      });
    }

    return (
      <div className="row">
        {clientState.parcels.parcelsData && (
          <div className="parcels-container">
            <div className="heading-and-map-container">
              <h1 className="heading-and-map-container__heading">{type}</h1>
              <Map multiParcelCoordinates={multiParcelCoordinates} />
            </div>

            {parcels.map((parcel: any, index: number) => {
              return <Parcel {...parcel} key={index} />;
            })}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  clientState: state.clientReducer,
  languageState: state.languageReducer,
});

export default connect(mapStateToProps)(withTranslation()(Parcels));
