/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { withTranslation, WithTranslation } from 'react-i18next';

type ClassesProps = WithTranslation;

const Classes = (props: ClassesProps) => {
  const { t } = props;

  return (
    <div className="row classes-container">
      <h1 className="classes-container__heading">{t('classes.title')}</h1>

      <p className="classes-container__paragraph">
        {t('classes.paragraphOne')}
      </p>

      <p className="classes-container__paragraph">
        {t('classes.paragraphTwo')}
      </p>

      <p className="classes-container__paragraph">
        {t('classes.paragraphThree')}{' '}
      </p>

      <p className="classes-container__paragraph">
        {t('classes.paragraphFour')}{' '}
        <a
          className="classes-container__link"
          target="blank"
          href="https://agridata.ec.europa.eu/extensions/FADNPublicDatabase/FADNPublicDatabase.html"
        >
          {t('classes.paragraphFourLinkText')}
        </a>
        .
      </p>

      <p className="classes-container__paragraph">
        {t('classes.paragraphFive')}
      </p>

      <p className="classes-container__paragraph">
        {t('classes.paragraphSix')}{' '}
        <a
          className="classes-container__link"
          target="blank"
          href={`${t('classes.paragraphSixLinkHref')}`}
        >
          {t('classes.paragraphSixLinkText')}
        </a>
        .
      </p>

      <p className="classes-container__paragraph">
        {t('classes.paragraphSeven')}
      </p>

      <h2 className="classes-container__secondary-heading">
        {t('classesFarmSpecTable.title')}
      </h2>

      <table className="table">
        <tbody>
          <tr className="table__row table__row--green">
            <th className="table__header-cell">
              {t('classesFarmSpecTable.headerOne')}
            </th>

            <th className="table__header-cell">
              {t('classesFarmSpecTable.headerTwo')}
            </th>
          </tr>
          <tr className="table__row">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellOneDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">15</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellOneDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">16</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellOneDataThree')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row  table__row--green">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellTwoDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">21</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellTwoDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">22</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellTwoDataThree')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">23</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellTwoDataFour')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellThreeDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">35</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellThreeDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">36</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellThreeDataThree')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">37</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellThreeDataFour')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">38</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellThreeDataFive')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellFourDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">45</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellFourDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">46</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellFourDataThree')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">47</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellFourDataFour')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">48</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellFourDataFive')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellFiveDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">51</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellFiveDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">52</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellFiveDataThree')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellSixDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">61</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellSixDataTwo')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellSevenDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">73</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellSevenDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">74</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellSevenDataThree')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellEightDataOne')}
            </td>

            <td>
              <table>
                <tbody>
                  <tr className="table__row">
                    <td className="table__data-cell">83</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellEightDataTwo')}
                    </td>
                  </tr>

                  <tr className="table__row">
                    <td className="table__data-cell">84</td>
                    <td className="table__data-cell table__data-cell--description">
                      {t('classesFarmSpecTable.dataCellEightDataThree')}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">
              {t('classesFarmSpecTable.dataCellNineDataOne')}
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>

      <h2 className="classes-container__secondary-heading">
        {t('classesSizeGroupingTable.title')}
      </h2>

      <table className="table table--center">
        <tbody>
          <tr className="table__row table__row--green">
            <th className="table__header-cell">
              {t('classesSizeGroupingTable.headerOne')}
            </th>
            <th className="table__header-cell">
              {t('classesSizeGroupingTable.headerTwo')}
            </th>
            <th className="table__header-cell">
              {t('classesSizeGroupingTable.headerThree')}
            </th>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">1</td>
            <td className="table__data-cell"></td>
            <td className="table__data-cell">1 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">2</td>
            <td className="table__data-cell">2 000</td>
            <td className="table__data-cell">3 999</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">3</td>
            <td className="table__data-cell">4 000</td>
            <td className="table__data-cell">7 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">4</td>
            <td className="table__data-cell">8 000</td>
            <td className="table__data-cell">14 999</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">5</td>
            <td className="table__data-cell">15 000</td>
            <td className="table__data-cell">24 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">6</td>
            <td className="table__data-cell">25 000</td>
            <td className="table__data-cell">49 999</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">7</td>
            <td className="table__data-cell">50 000</td>
            <td className="table__data-cell">99 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">8</td>
            <td className="table__data-cell">100 000</td>
            <td className="table__data-cell">249 999</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">9</td>
            <td className="table__data-cell">250 000</td>
            <td className="table__data-cell">499 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">10</td>
            <td className="table__data-cell">500 000</td>
            <td className="table__data-cell">749 999</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">11</td>
            <td className="table__data-cell">750 000</td>
            <td className="table__data-cell">999 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">12</td>
            <td className="table__data-cell">1 000 000</td>
            <td className="table__data-cell">1 499 999</td>
          </tr>

          <tr className="table__row">
            <td className="table__data-cell">13</td>
            <td className="table__data-cell">1 500 000</td>
            <td className="table__data-cell">2 999 999</td>
          </tr>

          <tr className="table__row table__row--green">
            <td className="table__data-cell">14</td>
            <td className="table__data-cell">3 000 000</td>
            <td className="table__data-cell"></td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default withTranslation()(Classes);
