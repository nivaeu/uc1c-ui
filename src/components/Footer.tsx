/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { connect } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { withTranslation, WithTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { setLanguage } from '../actions/languageActions';
import { languageReducerDefaultState } from '../reducers/languageReducer';
import { RootState, AppDispatch } from '../App';
import { countCharacters, calculatePatternIndex } from '../utils/appUtils';

const currentYear = new Date().getFullYear();
const languages = [
  {
    name: 'Eesti',
    code: 'et',
  },
  {
    name: 'English (US)',
    code: 'en',
  },
];

type FooterProps = WithTranslation & {
  dispatch: AppDispatch;
  languageState: typeof languageReducerDefaultState;
};

const getPathKey = (path: string) => {
  if (path === '/about' || path === '/meist') {
    return 'about';
  } else if (
    path === '/majanduslikud-klassid' ||
    path === '/economic-classes'
  ) {
    return 'classes';
  } else if (path === '/livestock-unit' || path === '/loomuhik') {
    return 'livestock';
  } else if (path.includes('klient') || path.includes('client')) {
    return 'client';
  }
};

const Footer = (props: FooterProps) => {
  const history = useHistory();
  const { pathname } = useLocation();

  const handleLanguageSwitch = (e: React.FormEvent<HTMLLIElement>) => {
    if (e.currentTarget.dataset.id) {
      props.i18n.changeLanguage(e.currentTarget.dataset.id);
      props.dispatch(setLanguage({ language: e.currentTarget.dataset.id }));

      if (history.location.pathname !== '/') {
        const pathKey = getPathKey(pathname);
        const pathSlashCount = countCharacters('/', pathname);

        if (pathSlashCount > 1) {
          const urlFirstPart = t(`paths.${pathKey}`);
          const secondSlashIndex = calculatePatternIndex(pathname, '/', 2);
          const urlSecondPart = pathname.substring(secondSlashIndex);

          history.push(`${urlFirstPart}${urlSecondPart}`);
        } else {
          history.push(t(`paths.${pathKey}`));
        }
      }
    }
  };

  const { t } = props;
  return (
    <footer className="footer">
      <ul className="language-list">
        {languages.map((language, index) => {
          return (
            <li
              className={
                props.languageState.language === language.code
                  ? 'language-list__item language-list__item--selected'
                  : 'language-list__item'
              }
              data-id={language.code}
              onClick={handleLanguageSwitch}
              key={index}
            >
              {language.name}
            </li>
          );
        })}
      </ul>

      <p className="footer__copyright footer__copyright--first">
        {t('footer.funding')}.
      </p>
      <p className="footer__copyright">
        © {currentYear} NIVA. {t('footer.copyright')}.
      </p>
    </footer>
  );
};

const mapStateToProps = (state: RootState) => ({
  languageState: state.languageReducer,
});

export default connect(mapStateToProps)(withTranslation()(Footer));
