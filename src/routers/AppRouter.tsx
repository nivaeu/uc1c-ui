/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { withTranslation, WithTranslation } from 'react-i18next';
import Header from '../components/Header';
import Footer from '../components/Footer';
import About from '../components/About';
import ClientSearch from '../components/ClientSearch';
import NotFound from '../components/NotFound';
import ClientView from '../components/ClientView';
import Classes from '../components/Classes';
import Livestock from '../components/Livestock';
import Parcels from '../components/Parcels';

type AppRouterProps = WithTranslation;

const AppRouter = (props: AppRouterProps) => {
  const { t } = props;
  return (
    <BrowserRouter>
      <div className="wrapper">
        <Header />
        <Switch>
          <Route path="/" exact={true} component={ClientSearch} />
          <Route path={`${t('paths.about')}`} component={About} />
          <Route
            path={`${t('paths.client')}/:id`}
            component={ClientView}
            exact={true}
          />
          <Route path={`${t('paths.classes')}`} component={Classes} />
          <Route path={`${t('paths.livestock')}`} component={Livestock} />
          <Route
            path={`${t('paths.client')}/:id${t('paths.parcels')}`}
            component={Parcels}
          />
          <Route component={NotFound} />
        </Switch>
      </div>
      <Footer />
    </BrowserRouter>
  );
};

export default withTranslation()(AppRouter);
