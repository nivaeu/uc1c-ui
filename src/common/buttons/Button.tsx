/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Button as MaterialButton,
  ButtonProps as MaterialButtonProps,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  button: {
    textTransform: 'none',
    color: '#fff',
    backgroundColor: '#07a84c',
    fontSize: '1.5rem',
    width: '100%',
    '&:hover': {
      backgroundColor: '#15c25f',
    },
  },
}));

export type ButtonProps = MaterialButtonProps;

const Button = (props: ButtonProps) => {
  const { ...buttonProps } = props;
  const classes = useStyles();

  return <MaterialButton className={classes.button} {...buttonProps} />;
};

export default Button;
