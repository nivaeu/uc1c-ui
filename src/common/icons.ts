/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import collapseIcon from '../assets/icon-collapse.svg';
import expandIcon from '../assets/icon-expand.svg';
import closeIcon from '../assets/icon-close.svg';

export { collapseIcon, expandIcon, closeIcon };
