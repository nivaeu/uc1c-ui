/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { ReactElement } from 'react';
import {
  TextField as MaterialTextField,
  TextFieldProps as MaterialTextFieldProps,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  input: {
    width: '27rem',
    '& .MuiInputBase-root': {
      fontSize: '1.5rem',
    },
    '& .MuiFormLabel-root': {
      fontSize: '1.5rem',
    },
    '& .MuiFormLabel-root.Mui-focused': {
      color: '#07a84c',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#B8B8B8',
      },
      '&:hover fieldset': {
        borderColor: '#B8B8B8',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#07a84c',
      },
    },
  },
  error: {
    fontSize: '1rem',
    position: 'absolute',
    bottom: '-2rem',
  },
}));

export type TextFieldProps = MaterialTextFieldProps & {
  inputProps?: any;
  params?: any;
};

const TextField = (props: TextFieldProps): ReactElement => {
  const { inputProps, params, ...fieldProps } = props;
  const classes = useStyles();

  return (
    <div>
      {params ? (
        <MaterialTextField
          {...params}
          className={classes.input}
          InputProps={{
            ...props.InputProps,
            ...params.InputProps,
          }}
          FormHelperTextProps={{
            classes: { error: classes.error },
          }}
          {...fieldProps}
        />
      ) : (
        <MaterialTextField
          className={classes.input}
          InputProps={{
            ...props.InputProps,
          }}
          FormHelperTextProps={{
            classes: { error: classes.error },
          }}
          {...fieldProps}
        />
      )}
    </div>
  );
};

export default TextField;
