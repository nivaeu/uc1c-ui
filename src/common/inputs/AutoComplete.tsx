/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import {
  Autocomplete as MaterialAutocomplete,
  AutocompleteProps as MaterialAutocompleteProps,
} from '@material-ui/lab';
import { Typography } from '@material-ui/core';

type AutocompleteProps = MaterialAutocompleteProps<string, true, true, true>;

const Autocomplete = (props: AutocompleteProps) => {
  return (
    <MaterialAutocomplete
      {...props}
      freeSolo
      disableClearable
      renderOption={(option) => (
        <Typography style={{ fontSize: '1.5rem' }}>{option}</Typography>
      )}
    />
  );
};

export default Autocomplete;
