/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { IconButton, Tooltip as MaterialTooltip } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  icon: {
    color: (props: ToolTipProps) => (props.color ? props.color : '#1e9e56'),
    padding: 0,
    '& .MuiSvgIcon-root': {
      fontSize: '1.75rem',
    },
  },
  toolTip: {
    background: '#1e9e56',
    fontSize: '1.55rem',
  },
  toolTipArrow: {
    color: '#1e9e56',
  },
}));

type ToolTipProps = {
  title: JSX.Element;
  open: boolean;
  handleToolTip: () => void;
  ariaLabel: string;
  color?: string;
};

const Tooltip = (props: ToolTipProps) => {
  const { title, open, handleToolTip, ariaLabel } = props;
  const classes = useStyles(props);

  return (
    <MaterialTooltip
      interactive
      PopperProps={{
        disablePortal: true,
      }}
      title={title}
      onClose={handleToolTip}
      open={open}
      disableFocusListener
      disableHoverListener
      disableTouchListener
      arrow={true}
      placement="right"
      classes={{
        tooltip: classes.toolTip,
        arrow: classes.toolTipArrow,
      }}
    >
      <IconButton
        className={classes.icon}
        onClick={handleToolTip}
        disableRipple
        aria-label={ariaLabel}
      >
        <InfoIcon />
      </IconButton>
    </MaterialTooltip>
  );
};

export default Tooltip;
