/*
Copyright (c) Estonian Agricultural Registers and Information Board,Tartu University 2019 -- 2022.
This file belongs to subproject UC1c of project NIVA (www.niva4cap.eu)
All rights reserved

Project and code is made available under the EU-PL v 1.2 license.
*/

import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import { languageReducer } from '../reducers/languageReducer';
import { clientReducer } from '../reducers/clientReducer';
import { chartReducer } from '../reducers/chartReducer';

const configureStore = () => {
  const store = createStore(
    combineReducers({
      languageReducer: languageReducer,
      clientReducer: clientReducer,
      chartReducer: chartReducer,
    }),
    composeWithDevTools(applyMiddleware(thunk)),
  );

  return store;
};

export default configureStore;
